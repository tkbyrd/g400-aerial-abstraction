﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    //TO DO FOR PLAYER CONTROLS:
    //
    //Move "forward" every frame.
    //Change up, down, left, and right direction upon key input.
    //Leave behind a visual trail. On and off switch?

    private Rigidbody playerRigidbody;
    private bool isFlying = true;

    public float horizontalSpeed;
    public float verticalSpeed;
    public float forwardSpeed;

    public float rotateSpeed;

    // Use this for initialization
    void Start ()
    {

        playerRigidbody = GetComponent<Rigidbody>();

    }

    // Update is called once per frame
    //void Update ()
    //{

    //}


    //STATUS: Needs fine-tuning, but functions currently.
    //IDEAS : Consider re-setting the vertical angle after input has been idle / slowly setting it to zero so that tilted flying isn't an issue?
    //        
    void FixedUpdate()
    {
        //If the player is flying...
        if (isFlying)
        {
            //Set the gravity of the player to false.
            this.playerRigidbody.useGravity = false;
        }

        //Controlling Direction:

        //If the player presses "D"
        if (Input.GetKey(KeyCode.D))
        {
            //Rotate the player rightward at the current rotation speed.
            transform.Rotate(transform.up, rotateSpeed, Space.World);
        }

        //If the player presses "A"
        if (Input.GetKey(KeyCode.A))
        {
            //Rotate the player leftward at the current rotation speed.
            transform.Rotate(transform.up, -1 * rotateSpeed, Space.World);
        }

        //If the player presses "W"
        if (Input.GetKey(KeyCode.W))
        {
            //Rotate the player upward at the current rotation speed.
            transform.Rotate(transform.right, rotateSpeed, Space.World);
        }

        //If the player presses "S"
        if (Input.GetKey(KeyCode.S))
        {
            //Rotate the player downward at the current rotation speed.
            transform.Rotate(transform.right, -1 * rotateSpeed, Space.World);
        }

        //Move the player in the direction they're facing using the constant foward speed.
        this.transform.Translate(this.transform.forward * forwardSpeed, Space.World);
    }
}
